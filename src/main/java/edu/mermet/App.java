package edu.mermet;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCombination;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
	var border = new BorderPane();
	var mFichier = new Menu("_Fichier");
	var miQuitter = new MenuItem("_Quitter");
	miQuitter.setAccelerator(new KeyCharacterCombination("Q", KeyCombination.CONTROL_DOWN));
	miQuitter.setOnAction(ev -> System.exit(0));
	mFichier.getItems().add(miQuitter);
	var mEdition = new Menu("_Édition");
	var miAnnuler = new MenuItem("_Annuler");
	var miRefaire = new MenuItem("_Refaire");
	var miDivers = new MenuItem("_Divers");
	mEdition.getItems().addAll(miAnnuler, miRefaire, new SeparatorMenuItem(),  miDivers);
	var barre = new MenuBar(mFichier, mEdition);
	border.setTop(barre);

        var scene = new Scene(border, 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
